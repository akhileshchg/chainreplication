/******************************************************
Team Members:
Akhilesh Chaganti:109648086	
Raghavendra Suvvari: 109596686

Client pseudo code
*********************************************************/


Client Code
===========
Every Client has three threads, one main thread to send requests, one to listen
on servers and one to  listen on master

Main Client thread
==================

bankName = <From Config File>
client = <Struct containing name, ip, port From Config File>
seqId = 0 #Initially initialized to 0 and updated for every request to server

resultQueue = NULL #Server listener thread reads the messages from head and 
				   #keeps them in this queue which maintains elements of type Result
				   
head, tail = NULL #Master listener thread reads the messages from master and 
				  #updates head and tail
				  
master = <From Config File>
sendMaster(master, client, bankName)
wait(to)
	
	
#This function is used to send the account details with reqId to the server for 
#depositing the amount in the account.	

function deposit(accountNum, amount):	
	reqId = bankName.(client.name).seq
	#updating the seq for every client by incrementing the 
	#previous value with a lock on it
	seq = seq + 1	
	while (1):
		buffer = "deposit", client, <reqId, accountNum, amount>
		if head is NULL:
			return Error	#if head doesn't exist then return error
		#sending the details to the server for its processing and wait for its response
		sendServer(head, buffer)	
		wait(T)
		if checkQueue(reqId):
			return Popqueue(reqId)
		
#This function is used to send the account details with reqId to the server for 
#withdrawing the amount from the account.			

function withdraw(accountNum, amount):
	reqId = bankName.(client.name).seq
	seq = seq + 1
	while (1):
		buffer = "withdraw", client, <reqId, accountNum, amount>
		if head is NULL:
			return Error
		sendServer(head, buffer)
		wait(T)
		if checkQueue(reqId):
			return Popqueue(reqId)


#This function is used to send the account details with reqId to the server for 
#querying the balance from the account.			
function query(accountNum):
	reqId = bankName.(client.name).seq
	seq = seq + 1
	while (1):
		buffer = "query", client, <reqId, accountNum>
		if tail is NULL:		##if tail doesn't exist then return error
			return Error
		sendServer(tail, buffer)
		wait(T)
		result = checkQueue(reqId)
		if  result not NULL:
			return resultQueue.pop(result)

#This is used to search the queue with reqId for the result among all the result
#data obtained from the server			
function checkQueue(reqId)
	for all result in resultQueue:
		if reqId matches result.reqId:
			return resultQueue.index(result)
	return NULL

Thread listener for master:
===========================
while(1)
	localBuffer = recvMaster(master) #waits until there is message from master
	head,tail = getServers(localBuffer)

Thread listner for server:
==========================
while(1)
	buffer = recvServer(tail)
	Reply reply = getReply(buffer)
	resultQueue.append(reply)
------------------------------------------------------------------------
