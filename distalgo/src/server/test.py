#!/usr/local/bin/python3.4
import threading
import time
def threadfunc(stop):
	print("started")
	while (not stop.isSet()):
		print('ping received')
		time.sleep(1)
	print("ended")

if __name__ == '__main__':
	stop = threading.Event()
	t = threading.Thread(target=threadfunc, args=(stop,))
	t.start()
	time.sleep(10)
	stop.set()
	print("thread ended. now exiting main")
