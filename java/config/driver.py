#!/usr/local/bin/python3.4
import sys
from configobj import ConfigObj
import subprocess as s
if __name__ == '__main__': 
	if len(sys.argv) != 3:
		print ("usage: ./driver.py <server.jar location> <config file location>")
		exit()
	server_binary=sys.argv[1]
	config_file = sys.argv[2]

	print('running java -jar '+server_binary+' '+server_binary)
	configHandle = ConfigObj(sys.argv[2])
	numBanks = int(configHandle['GENERIC_BANK_DETAILS']['NUM_BANKS'])
	print(numBanks)
	proc = list()
	for bank in range(numBanks):
		chain_len = int(configHandle['BANK_'+str(bank+1)]['CHAIN_LEN'])
		print(chain_len)
		for server in range(chain_len):
			p = s.Popen(['java', '-jar', server_binary, config_file, str(bank+1), str(server+1)], stdout=s.PIPE, stdin=s.PIPE, stderr=s.PIPE)
			proc.append(p)
	for p in proc:
		print(p.communicate())		
