package common;

import java.net.InetAddress;

public class SyncRequest extends Request{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5995450541690286217L;
	//public static final String sync = "sync";
	private String mainOp;
	private String status;
	private int balance;
	private InetAddress clientInetAddress;
	private int clientPort;
	private int serverSequence;
	public SyncRequest(Request request, String status, int balance, InetAddress clientInetAddress, int clientPort, int check2 ){
		super("sync", request.getRequestID(), request.getAccount(), request.getAmount(), request.getSequenceId());
		setMainOp(request.getOp());
		setStatus(status);
		setBalance(balance);
		setClientInetAddress(clientInetAddress);
		setClientPort(clientPort);
		setServerSequence(check2);
	}
	public Request getRequest() {
		return new Request(this.getMainOp(), this.getRequestID(), 
				this.getAccount(), this.getAmount(), this.getSequenceId());
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the balance
	 */
	public int getBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(int balance) {
		this.balance = balance;
	}
	/**
	 * @return the clientInetAddress
	 */
	public InetAddress getClientInetAddress() {
		return clientInetAddress;
	}
	/**
	 * @param clientInetAddress2 the clientInetAddress to set
	 */
	public void setClientInetAddress(InetAddress clientInetAddress2) {
		this.clientInetAddress = clientInetAddress2;
	}
	/**
	 * @return the clientPort
	 */
	public int getClientPort() {
		return clientPort;
	}
	/**
	 * @param clientPort the clientPort to set
	 */
	public void setClientPort(int clientPort) {
		this.clientPort = clientPort;
	}
	public int getServerSequence() {
		return serverSequence;
	}
	public void setServerSequence(int check) {
		this.serverSequence = check;
	}
	public String getMainOp() {
		return mainOp;
	}
	public void setMainOp(String mainOp) {
		this.mainOp = mainOp;
	}
}

