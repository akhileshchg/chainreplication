package common;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import client.Client;

import org.ini4j.*;

public class ChainReplication {

	/*
	 * For every bank read the details the client needs:
	 * bankName, number of requests, wait time, number of retries
	 * requests description, head and tail of the chain.
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if (args.length != 1){
			throw new ExceptionInInitializerError("No Config file given");
		}
		try {
			Ini configHandle = new Ini(new File(args[0]));
			int numBanks = Integer.parseInt(configHandle.get("GENERIC_BANK_DETAILS", "NUM_BANKS"));
			int[] numClientsList = new int[numBanks];
			for (int bank = 0; bank < numBanks; bank++) {
				int numClients = Integer.parseInt(configHandle.get("BANK_"+Integer.toString(bank+1), "NUM_CLIENTS"));
				numClientsList[bank] = numClients;
			}
			List<Client> clientList = new ArrayList<Client>();
			System.out.println(numBanks);
			for (int bank = 0; bank < numBanks; bank++){
				String bankName = configHandle.get("BANK_"+Integer.toString(bank+1), "BANK_NAME");
				int chainLen = Integer.parseInt(configHandle.get("BANK_"+Integer.toString(bank+1), "CHAIN_LEN"));
				String headIp = configHandle.get("BANK_"+Integer.toString(bank+1), "IP_1");
				int headUdpPort = Integer.parseInt(configHandle.get("BANK_"+Integer.toString(bank+1), "SERVER_UDP_PORT_1"));
				String tailIp = configHandle.get("BANK_"+Integer.toString(bank+1), "IP_"+Integer.toString(chainLen));
				int tailUdpPort = Integer.parseInt(configHandle.get("BANK_"+Integer.toString(bank+1), "SERVER_UDP_PORT_"+Integer.toString(chainLen)));
				
				for (int client = 0; client <numClientsList[bank]; client++){
					String clientIp = configHandle.get("BANK_"+Integer.toString(bank+1), "CLIENT_IP_"+Integer.toString(client+1));
					int clientUdpPort = Integer.parseInt(configHandle.get("BANK_"+Integer.toString(bank+1), "CLIENT_UDP_PORT_"+Integer.toString(client+1)));
					int sleepTime = 0;
					try {
						sleepTime = Integer.parseInt(configHandle.get("BANK_"+Integer.toString(bank+1), "SLEEP_ON_REQUEST_"+Integer.toString(client+1)));
					}catch (NumberFormatException e) {
						sleepTime =  0;
					}
					int numReqs = Integer.parseInt(configHandle.get("BANK_"+Integer.toString(bank+1), "NUM_REQS_"+Integer.toString(client+1)));
					String reqType = configHandle.get("BANK_"+Integer.toString(bank+1), "REQUEST_TYPE_"+Integer.toString(client+1));
					RequestDescriptor reqDesc;
					if (reqType.equals("random")){
						long seed = Long.parseLong(configHandle.get("BANK_"+Integer.toString(bank+1), "SEED_"+Integer.toString(client+1)));
						float probDeposit = Float.parseFloat(configHandle.get("BANK_"+Integer.toString(bank+1), "PROB_DEPOSIT_"+Integer.toString(client+1)));
						float probWithdraw = Float.parseFloat(configHandle.get("BANK_"+Integer.toString(bank+1), "PROB_WITHDRAW_"+Integer.toString(client+1)));
						float probQuery = Float.parseFloat(configHandle.get("BANK_"+Integer.toString(bank+1), "PROB_QUERY_"+Integer.toString(client+1)));
						String[] accounts = configHandle.get("BANK_"+Integer.toString(bank+1), "ACCOUNTS_"+Integer.toString(client+1)).split(",");
						String[] amountsStr = configHandle.get("BANK_"+Integer.toString(bank+1), "AMOUNTS_"+Integer.toString(client+1)).split(",");
						int[] amounts = new int[amountsStr.length];
						for (int i = 0; i < amountsStr.length; i++) {
							amounts[i] = Integer.parseInt(amountsStr[i]);
						}
						reqDesc = new RandomRequestDescriptor(seed, probDeposit, probWithdraw, probQuery, accounts, amounts);						
					} else if (reqType.equals("itemized")){
						int override = Integer.parseInt(configHandle.get("BANK_"+Integer.toString(bank+1), "REQUEST_ID_OVERRIDE_"+Integer.toString(client+1)));
						List<Request> requestList = parseRequestList(configHandle.get("BANK_"+Integer.toString(bank+1), "REQUESTS_"+Integer.toString(client+1)), override);
						reqDesc = new ItemizedRequestDescriptor(override, requestList);
						numReqs = requestList.size();
					} else{
						throw new ExceptionInInitializerError("Invalid value: use random or itemized");
					}
					int waitTime = Integer.parseInt(configHandle.get("BANK_"+Integer.toString(bank+1), "WAIT_"+Integer.toString(client+1)));
					int retries = Integer.parseInt(configHandle.get("BANK_"+Integer.toString(bank+1), "RETRYS_"+Integer.toString(client+1)));
					//System.out.println(headIp+' '+headUdpPort+' '+tailIp+' '+tailUdpPort+' '+clientIp+' '+clientUdpPort);
					//System.out.println("Number of requests = "+numReqs);
					Client clientThread = new Client(bankName, numReqs, reqDesc, waitTime, retries, headIp, tailIp, headUdpPort, tailUdpPort, clientIp, clientUdpPort, sleepTime);
					clientThread.setName(bankName+"-client-"+Integer.toString(client));
					clientList.add(clientThread);
				}
			}
			for(Client clientThread : clientList){
				clientThread.start();
			}
			for(Client clientThread: clientList){
				clientThread.join();
			}
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static List<Request> parseRequestList(String string, int override) {
		// TODO Auto-generated method stub
		String[]requests = string.split("\\|");
		List<Request> reqList = new ArrayList<Request>();
		for(int i = 0; i < requests.length;i++){
			StringTokenizer tokenizer = new StringTokenizer(requests[i],"; ()");
			int count = tokenizer.countTokens();
			String[] request = new String[count];
			for(int j = 0; tokenizer.hasMoreTokens(); j++){
				request[j] = tokenizer.nextToken();
			}
			int amount = 0;
			if (override == 1){
				if (!request[0].equals("query")){
					amount = Integer.parseInt(request[3]);
				}
				reqList.add(new Request(request[0], request[1], request[2], amount));
			} else if (override == 0) {
				if (!request[0].equals("query")){
					amount = Integer.parseInt(request[2]);
				}
				reqList.add(new Request(request[0], request[1], amount));
			} else{
				throw new ExceptionInInitializerError("Bad Value for override: 0 or 1 allowed");
			}
		}
		return reqList;
	}
}

