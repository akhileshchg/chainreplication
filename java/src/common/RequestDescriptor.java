package common;

public abstract class RequestDescriptor {
	private String reqType;
	public abstract Request getRequest(String bankName, String clientID);
	/**
	 * @return the reqType
	 */
	public String getReqType() {
		return reqType;
	}
	/**
	 * @param reqType the reqType to set
	 */
	public void setReqType(String reqType) {
		this.reqType = reqType;
	}
}