package common;

import java.util.Random;

public class RandomRequestDescriptor extends RequestDescriptor{
	private int seed;
	private float probDeposit;
	private float probWithdraw;
	private float probQuery;
	private String[] accounts;
	private int[] amounts;
	private Random random;
	private String[] operations;
	private int seq = 0;
	public RandomRequestDescriptor(long seed, float probDeposit, float probWithdraw, float probQuery, String[] accounts, int[] amounts) {
		// TODO Auto-generated constructor stub
		random = new Random();
		random.setSeed(seed);
		setProbDeposit(probDeposit);
		setProbWithdraw(probWithdraw);
		setProbQuery(probQuery);
		setAccounts(accounts);
		setAmounts(amounts);
		setReqType("random");
		int numDeposits = (int)(probDeposit*10.0);
		int numWithdraws = (int) (probWithdraw*10.0);
		int numQueries = (int) (probQuery*10.0);
		operations = new String[10];
		for(int i = 0; i < numDeposits; i++){
			operations[i] = "deposit";
		}
		for(int i = 0; i < numWithdraws; i++){
			operations[numDeposits+i] = "withdraw";
		}
		for(int i = 0; i < numQueries; i++){
			operations[numDeposits+numWithdraws+i] = "query";
		}
	}

	public Request getRequest(String bankName, String clientID){
		System.out.println("Generating Random Request");
		String op = operations[random.nextInt(10)];
		String requestID = bankName+"."+clientID+"."+Integer.toString(seq);
		String account = accounts[random.nextInt(accounts.length)];
		Request request = new Request(op, requestID, account, (long)seq);
		if (!op.equals("query"))
			request.setAmount(amounts[random.nextInt(amounts.length)]);
		seq++;
		return request;
	}

	/**
	 * @return the seed
	 */
	public int getSeed() {
		return seed;
	}

	/**
	 * @param seed the seed to set
	 */
	public void setSeed(int seed) {
		this.seed = seed;
	}

	/**
	 * @return the probDeposit
	 */
	public float getProbDeposit() {
		return probDeposit;
	}

	/**
	 * @param probDeposit the probDeposit to set
	 */
	public void setProbDeposit(float probDeposit) {
		this.probDeposit = probDeposit;
	}

	/**
	 * @return the probWithdraw
	 */
	public float getProbWithdraw() {
		return probWithdraw;
	}

	/**
	 * @param probWithdraw the probWithdraw to set
	 */
	public void setProbWithdraw(float probWithdraw) {
		this.probWithdraw = probWithdraw;
	}

	/**
	 * @return the probQuery
	 */
	public float getProbQuery() {
		return probQuery;
	}

	/**
	 * @param probQuery the probQuery to set
	 */
	public void setProbQuery(float probQuery) {
		this.probQuery = probQuery;
	}

	/**
	 * @return the accounts
	 */
	public String[] getAccounts() {
		return accounts;
	}

	/**
	 * @param accounts the accounts to set
	 */
	public void setAccounts(String[] accounts) {
		this.accounts = new String[accounts.length];
		for (int i = 0; i < accounts.length; i++){
			this.accounts[i] = accounts[i]; 
		}
	}

	/**
	 * @return the amounts
	 */
	public int[] getAmounts() {
		return amounts;
	}

	/**
	 * @param amounts the amounts to set
	 */
	public void setAmounts(int[] amounts) {
		this.amounts = new int[amounts.length];
		for(int i = 0; i < amounts.length; i++) {
			this.amounts[i] = amounts[i];
		}
	}
}