package common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ItemizedRequestDescriptor extends RequestDescriptor{
	private int override;
	private List<Request> requestList;
	int seq = 0;
	public ItemizedRequestDescriptor(int override, List<Request> requestList) {
		// TODO Auto-generated constructor stub
		seq = 0;
		setOverride(override);
		setRequestList(requestList);
		setReqType("itemized");
	}
	public ItemizedRequestDescriptor(){
		
	}
	public Request getRequest(String bankName, String clientID){
		System.out.println("Itemized Request Descriptor");
		Request request = null;
		if (!requestList.isEmpty())
			request = requestList.remove(0);
			request.setSequenceId(seq);
			
			if (override == 0){
				String requestId = bankName+"."+clientID+"."+Integer.toString(seq);
				request.setRequestID(requestId);
			}
			seq++;
		return request;
	}
	/**
	 * @return the override
	 */
	public int getOverride() {
		return override;
	}
	/**
	 * @param override the override to set
	 */
	public void setOverride(int override) {
		this.override = override;
	}
	/**
	 * @return the requestList
	 */
	public List<Request> getRequestList() {
		return requestList;
	}
	/**
	 * @param requestList the requestList to set
	 */
	public void setRequestList(List<Request> requestList) {
		this.requestList = new ArrayList<Request>();
		for(Iterator<Request> iter = requestList.iterator();iter.hasNext();){
			this.requestList.add(iter.next());
		}
	}
	
}