package common;

import java.io.Serializable;

public class Request implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6882499596663395481L;
	private String op;
	private String requestID;
	private String account;
	private long sequenceId;
	private int amount;
	public Request(){
		setOp("");
		setRequestID("");
		setAccount("");
		amount = 0;
		setSequenceId(0);
	}
	public Request(String op) {
		setOp(op);
		setRequestID("");
		setAccount("");
		amount = 0;
		setSequenceId(0);
	}
	public Request(String op, String requestID, String account, long sequenceId){
		setOp(op);
		setRequestID(requestID);
		setAccount(account);
		amount = 0;
		setSequenceId(sequenceId);
	}
	public Request(String op, String requestID, String account, int amount, long sequenceId){
		setOp(op);
		setRequestID(requestID);
		setAccount(account);
		setAmount(amount);
		setSequenceId(sequenceId);
	}
	public Request(String op, String account, int amount){
		setOp(op);
		setRequestID("Default_ID");
		setAccount(account);
		setAmount(amount);
		setSequenceId(0L);
	}
	public Request(String op, String requestID, String account, int amount){
		setOp(op);
		setRequestID(requestID);
		setAccount(account);
		setAmount(amount);
		setSequenceId(0L);
	}
	/**
	 * @return the op
	 */
	public String getDetails(){
		if(op.equals("query"))
			return "Operation = "+getOp()+" Request ID = "+getRequestID()+" Account = "+getAccount();
		else
			return "Operation = "+getOp()+" Request ID = "+getRequestID()+" Account = "+getAccount()+" Amount = "+getAmount();
	}
	public String getOp() {
		return op;
	}
	/**
	 * @param op the op to set
	 */
	public void setOp(String op) {
		this.op = op;
	}
	/**
	 * @return the requestID
	 */
	public String getRequestID() {
		return requestID;
	}
	/**
	 * @param requestID the requestID to set
	 */
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}
	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}
	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}
	/**
	 * @param amount2 the amount to set
	 */
	public void setAmount(int amount2) {
		this.amount = amount2;
	}
	/**
	 * @return the sequenceId
	 */
	public long getSequenceId() {
		return sequenceId;
	}
	/**
	 * @param sequenceId the sequenceId to set
	 */
	public void setSequenceId(long sequenceId) {
		this.sequenceId = sequenceId;
	}
}