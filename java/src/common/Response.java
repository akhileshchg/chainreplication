package common;

import java.io.Serializable;

public class Response implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -570224693278831430L;
	private String op;
	private String requestID;
	private String account;
	private long sequenceId;
	private int balance;
	private String status;
	public Response() {
		setOp("");
		setRequestID("");
		setAccount("");
		setBalance(0);
		setSequenceId(0);
		setStatus("");
	}
	public Response(String op, String requestID, String account, String status, int balance, long sequenceId){
		setOp(op);
		setRequestID(requestID);
		setAccount(account);
		setBalance(balance);
		setSequenceId(sequenceId);
		setStatus(status);
	}
	public Response(Request request, String status, int balance){
		setOp(request.getOp()+"_ack");
		setRequestID(request.getRequestID());
		setAccount(request.getAccount());
		setBalance(balance);
		setSequenceId(request.getSequenceId());
		setStatus(status);
	}
	public Response(SyncRequest request){
		setOp(request.getMainOp()+"_ack");
		setRequestID(request.getRequestID());
		setAccount(request.getAccount());
		setBalance(request.getBalance());
		setSequenceId(request.getSequenceId());
		setStatus(request.getStatus());
	}
	/**
	 * @return the op
	 */
	public String getOp() {
		return op;
	}
	/**
	 * @param op the op to set
	 */
	public void setOp(String op) {
		this.op = op;
	}
	/**
	 * @return the requestID
	 */
	public String getRequestID() {
		return requestID;
	}
	/**
	 * @param requestID the requestID to set
	 */
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}
	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}
	/**
	 * @return the sequenceId
	 */
	public long getSequenceId() {
		return sequenceId;
	}
	/**
	 * @param sequenceId the sequenceId to set
	 */
	public void setSequenceId(long sequenceId) {
		this.sequenceId = sequenceId;
	}
	/**
	 * @return the balance
	 */
	public int getBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(int balance) {
		this.balance = balance;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
