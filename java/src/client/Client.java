package client;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import master.MasterResponse;
import common.*;
public class Client extends Thread {
	private String bankName;
	private int numRequests;
	private int waitTime;
	private int retries;
	private RequestDescriptor requestDescriptor;
	private String clientIp;
	private String headIp;
	private String tailIp;
	private int clientUdpPort;
	private int headUdpPort;
	private int tailUdpPort;
	private DatagramSocket clientSocket = null;
	private int sleepTime;
	Logger logger = Logger.getLogger("Client log");  
    	FileHandler fh;
	public Client(String bankName, int numRequests, RequestDescriptor requestDescriptor,
			int waitTime, int retries, String headIp, String tailIp, int headUdpPort, int tailUdpPort,
			String clientIp, int clientUdpPort, int sleepTime) throws SocketException{
		this.bankName = bankName;
		this.numRequests = numRequests;
		this.waitTime = waitTime;
		this.retries= retries;
		this.requestDescriptor = requestDescriptor;
		this.headIp = headIp;
		this.tailIp = tailIp;
		this.headUdpPort = headUdpPort;
		this.tailUdpPort = tailUdpPort;
		this.clientIp = clientIp;
		this.clientUdpPort = clientUdpPort;
		this.sleepTime = sleepTime;
		setClientSocket(clientUdpPort);
	}
	public  void printClientDetails(){
		logger.info("Client Name "+this.getName());
	}
	public void run(){
		printClientDetails();
		try {
			fh = new FileHandler(this.getName()+"_"+Long.toString(System.currentTimeMillis()/1000L));  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  
	        logger.setUseParentHandlers(false);
	        logger.info("In Client - run method");
		} catch (SecurityException e) {  
			logger.info("Problem with creating log file");
	        e.printStackTrace(); 
	        System.exit(1);
	    	} catch (IOException e) {
	    	logger.info("IO Exception");
			e.printStackTrace();
			System.exit(1);
		} 
		for (int i = 0; i < numRequests; i++) {
			Request request = requestDescriptor.getRequest(bankName, this.getName());
			for (int attempt = 0; attempt < retries; attempt++) {
				boolean received = false;
				try {
					if (request.getOp().equals("query")) {
						logger.info("Sending Request to tail "+attempt);
						sendServer(request, false);
					} else {
						logger.info("Sending Request to head "+attempt);
						sendServer(request, true);
					}
					boolean repeat;
					do {
						//logger.info("Inside innner while");
						repeat = false;
						try {
							Response response = recvServer();
							if (response.getOp().equals("changeHead")) {
								logger.info("New head notification received at client");
								MasterResponse masterResponse = (MasterResponse) response;
								this.headIp = masterResponse.getIp();
								this.headUdpPort = masterResponse.getUdpPort();
								repeat = true;
							} else if (response.getOp().equals("changeTail")) {
								logger.info("New tail notification received at client");
								MasterResponse masterResponse = (MasterResponse) response;
								this.tailIp = masterResponse.getIp();
								this.tailUdpPort = masterResponse.getUdpPort();
								repeat = true;
							} else {
								if (response.getOp().equals(request.getOp()+"_ack") && response.getRequestID().equals(request.getRequestID())
										&& response.getAccount().equals(request.getAccount()) && 
										response.getSequenceId() == request.getSequenceId()) {
									logger.info("Response for request with RequestId "+request.getRequestID()+
											" ,Operation "+request.getOp()+" Account "+request.getAccount()+" with Amount "+request.getAmount()
											+"\nStatus = "+response.getStatus()+" Balance = "+response.getBalance());
									Thread.sleep(sleepTime*1000);
									received = true;
									break;
								}
							}
						} catch (SocketTimeoutException e) {
							logger.info("Timeout");
							break;
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}while(repeat);
					if (received)
						break;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	public DatagramPacket prepareRequestPacket(Request request, boolean head) throws IOException {
		ByteArrayOutputStream responseArrayStream = new ByteArrayOutputStream();
		ObjectOutputStream outStream = new ObjectOutputStream(responseArrayStream);
		outStream.writeObject(request);
		byte[] byteResponse = responseArrayStream.toByteArray();
		byte[] byteResponseStream = new byte[4096];
		System.arraycopy(byteResponse, 0, byteResponseStream, 0, byteResponse.length);
		if (head)
			return new DatagramPacket(byteResponseStream, byteResponseStream.length,
					InetAddress.getByName(headIp), headUdpPort);
		else
			return new DatagramPacket(byteResponseStream, byteResponseStream.length,
					InetAddress.getByName(tailIp), tailUdpPort);
	}
	private Response recvServer() throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		byte[] requestStream = new byte[4096];
		DatagramPacket requestPacket = new DatagramPacket(requestStream, requestStream.length);
		setClietSoTimeout(waitTime);
		getClientSocket().receive(requestPacket);
		ByteArrayInputStream requestArrayStream = new ByteArrayInputStream(requestStream);
		ObjectInputStream inStream = new ObjectInputStream(requestArrayStream);
		return (Response)inStream.readObject();
	}
	private void sendServer(Request request, boolean head) throws IOException {
		// TODO Auto-generated method stub
		DatagramPacket requestPacket = prepareRequestPacket(request, head);
		getClientSocket().send(requestPacket);
		
	}
	public DatagramSocket getClientSocket() {
		return clientSocket;
	}
	public void setClientSocket(int clientUdpSocket) throws SocketException {
		this.clientSocket = new DatagramSocket(clientUdpSocket);
	}
	public void closeClientSocket() {
		if (clientSocket!=null && !clientSocket.isClosed())
			clientSocket.close();
	}
	public void setClietSoTimeout(int i) throws SocketException {
		if (clientSocket!=null && !clientSocket.isClosed())
			clientSocket.setSoTimeout(i*1000);
	}
}
