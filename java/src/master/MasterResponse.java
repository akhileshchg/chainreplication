package master;

import common.Response;

public class MasterResponse extends Response{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6915936896860708548L;
	private String bankName;
	private String ip;
	private int udpPort;
	MasterResponse(String op, String bankName, String ip, int udpPort) {
		super(op,"","","",0,0);
		setBankName(bankName);
		setIp(ip);
		setUdpPort(udpPort);
	}
	//String op, String requestID, String account, String status, int balance, long sequenceId
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getUdpPort() {
		return udpPort;
	}
	public void setUdpPort(int udpPort) {
		this.udpPort = udpPort;
	}
	
}
