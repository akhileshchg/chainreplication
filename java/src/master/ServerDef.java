package master;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicBoolean;

public class ServerDef implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9107823732461345827L;
	private String ipbyname;
	private InetAddress ip = null;
	private int tcpPort;
	private int udpPort;
	private AtomicBoolean active = null;
	public ServerDef(String serverIp, int serverTcpPort, int serverUdpPort) throws UnknownHostException {
		// TODO Auto-generated constructor stub
		if (!serverIp.equals(""))
			setIp(InetAddress.getByName(serverIp));
		setIpByName(serverIp);
		setTcpPort(serverTcpPort);
		setUdpPort(serverUdpPort);
		setActive(false);
	}
	public ServerDef() {
		setIpByName("");
		setTcpPort(0);
		setUdpPort(0);
		setActive(false);
	}
	public InetAddress getIp() {
		return ip;
	}
	public void setIp(InetAddress ip) {
		this.ip = ip;
	}
	public int getTcpPort() {
		return tcpPort;
	}
	public void setTcpPort(int tcpPort) {
		this.tcpPort = tcpPort;
	}
	public int getUdpPort() {
		return udpPort;
	}
	public void setUdpPort(int udpPort) {
		this.udpPort = udpPort;
	}
	public boolean getActive() {
		return active.get();
	}
	public void setActive(boolean active) {
		if (this.active == null)
			this.active = new AtomicBoolean(active);
		else
			this.active.set(active);
	}
	public String getIpByName() {
		return ipbyname;
	}
	public void setIpByName(String ipbyname) {
		this.ipbyname = ipbyname;
	}

}
