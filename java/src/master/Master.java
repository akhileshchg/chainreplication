package master;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.ini4j.Ini;

import common.Request;
import server.ClientRequestDetails;
import server.MasterRequest;

public class Master {
	public final static Logger logger = Logger.getLogger("Master log"); 
	public static void main(String[] args) {
		try {
			FileHandler fh = new FileHandler("master"+"_"+Long.toString(System.currentTimeMillis()/1000L));  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  
	        logger.setUseParentHandlers(false);
	        logger.info("In Master - main method");
		} catch (SecurityException e) {  
			logger.info("Problem with creating log file");
	        e.printStackTrace(); 
	        System.exit(1);
	    	} catch (IOException e) {
	    	logger.info("IO Exception");
			e.printStackTrace();
			System.exit(1);
		} 
		// TODO Auto-generated method stub
		if (args.length != 1){
			throw new ExceptionInInitializerError("No Config file given");
			
		}
		Ini configHandle = null;
		try {
			configHandle = new Ini(new File(args[0]));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			logger.info("Problem with config file");
			e1.printStackTrace();
			System.exit(1);
		}
		MasterDetails details = null;
		try {
			String masterIp = configHandle.get("GENERIC_BANK_DETAILS", "MASTER_IP");
			int masterUdpPort = Integer.parseInt(configHandle.get("GENERIC_BANK_DETAILS", "MASTER_UDP_PORT"));
			int masterTcpPort = Integer.parseInt(configHandle.get("GENERIC_BANK_DETAILS", "MASTER_TCP_PORT"));
			int hbTimeout = Integer.parseInt(configHandle.get("GENERIC_BANK_DETAILS", "MASTER_CHECK_TIME"));
			int newTailTimeout = Integer.parseInt(configHandle.get("GENERIC_BANK_DETAILS", "MASTER_CHECK_TAIL_FAILURE_TIME"));
			int numBanks = Integer.parseInt(configHandle.get("GENERIC_BANK_DETAILS", "NUM_BANKS"));
			List<String> bankNames = new ArrayList<String>();
			List<List<ServerDef>> bankList = new ArrayList<List<ServerDef>>();
			List<List<ServerDef>> clientList = new ArrayList<List<ServerDef>>();
			for (int bank = 1; bank <=numBanks; bank++) {
				String currentBank = Integer.toString(bank);
				String bankName = configHandle.get("BANK_"+currentBank, "BANK_NAME");
				bankNames.add(bankName);
				List<ServerDef> serverChain = Collections.synchronizedList(new ArrayList<ServerDef>());
				//Add Server IP, TCP and UDP PORTs
				int chainLen = Integer.parseInt(configHandle.get("BANK_"+currentBank, "CHAIN_LEN"));
				for (int s = 1; s <= chainLen; s++) {
					String currentServer = Integer.toString(s);
					String serverIp = configHandle.get("BANK_"+currentBank, "IP_"+currentServer);
					int serverUdpPort = Integer.parseInt(configHandle.get("BANK_"+currentBank, "SERVER_UDP_PORT_"+currentServer));
					int serverTcpPort = Integer.parseInt(configHandle.get("BANK_"+currentBank, "SERVER_TCP_PORT_"+currentServer));
					ServerDef server = new ServerDef(serverIp, serverTcpPort, serverUdpPort);
					serverChain.add(server);
				}
				List<ServerDef> clientChain = new ArrayList<ServerDef>();
				int numClients = Integer.parseInt(configHandle.get("BANK_"+currentBank, "NUM_CLIENTS"));
				for (int s = 1; s <= numClients; s++) {
					String currentClient = Integer.toString(s);
					String serverIp = configHandle.get("BANK_"+currentBank, "CLIENT_IP_"+currentClient);
					int serverUdpPort = Integer.parseInt(configHandle.get("BANK_"+currentBank, "CLIENT_UDP_PORT_"+currentClient));
					//int serverTcpPort = Integer.parseInt(configHandle.get("BANK_"+currentBank, "CLIENT_TCP_PORT_"+currentClient));
					ServerDef client = new ServerDef(serverIp, 0, serverUdpPort);
					clientChain.add(client);
				}
				//Add Client IP, UDP PORTs
				bankList.add(serverChain);
				clientList.add(clientChain);
			}
			details = new MasterDetails(bankNames, bankList, clientList, 
										hbTimeout, newTailTimeout,
										masterIp, masterTcpPort, masterUdpPort, logger);
		} catch (IOException e) {
			logger.info("Error before setting up all the sockets");
			e.printStackTrace();
			System.exit(1);
		}
		if (details == null) {
			logger.info("Error in setting up master");
			System.exit(0);
		}
		PingListner plisten = new PingListner(details);
		plisten.start();
		RequestListner rlistener = new RequestListner(details);
		rlistener.start();
		while(true) {
			try {
				Thread.sleep(details.getHbTimeout()*1000);
				for(int bank = 0; bank < details.getNumbanks(); bank++) {
					if (details.getExtension(bank) && 
							(System.currentTimeMillis()/1000L)-details.start[bank] > details.getNewTailTimeout()) {
						logger.info("No message from new Server. Sending old Tail to regain its status");
						details.sendServer("newTail", bank, details.getTailIndex(bank));
						details.sendClients("changeTail", bank, details.getTailIndex(bank));
						details.setExtension(bank, false);
						details.currentNewTail.set(bank, null);
						details.currentRequest.set(bank,null);
						details.start[bank] = 0;
						details.numTry[bank] = 0;
					}
					if (details.getBankList().get(bank).size() != 0) {
						if (details.getBankList().get(bank).get(0).getActive()
								== false) {
							if (details.containsActive(bank, true)) {
								details.setNewHead(bank);
								details.sendServer("newHead", bank, 0);
								details.sendClients("changeHead", bank,0);
							} else {
								details.removeServerChain(bank);
							}
						} else {
							//logger.info("Head of "+
								//	details.getBankNames().get(bank)+""
									//+ "is good");
						}
					}
					if (details.getBankList().get(bank).size() != 0) {
						details.printActiveState();
						if (details.getTail(bank)!=null && details.getTail(bank).getActive()
								== false) {
							if (details.containsActive(bank, true)) {
								details.setNewTail(bank);
								logger.info("Changing tail");
								details.printActiveState();
								details.sendServer("newTail", bank, details.getTailIndex(bank));
								details.sendClients("changeTail", bank, details.getTailIndex(bank));
								if (details.getExtension(bank)) {
									details.extendTail(bank, details.currentRequest.get(bank));
								}
							} else {
								details.removeServerChain(bank);
							}
						}
						 else {
								logger.info("Tail of "+
										details.getBankNames().get(bank)+""
										+ "is good");
								
							}
					}
					if (details.getBankList().get(bank).size() != 0) {
						if (details.containsActive(bank, true)) {
							while (details.containsActive(bank, false)) {
								logger.info("Master found failed server");
								int i = 0;
								details.printActiveState();
								int index = details.getIndexActive(bank, i, false);
								logger.info(Integer.toString(index));
								details.removeFailureNodes(bank,index);
								logger.info("Master sending message to successor of failed Node");
								details.printActiveState();
								details.sendServer("newSuccessor", bank, index);
							}
						} else {
							details.removeServerChain(bank);
						}
					}
				}
				//details.printActiveState();
				details.setAllActive(false);
				//details.printActiveState();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

}
class RequestListner extends Thread{
	private MasterDetails details;
	RequestListner(MasterDetails details) {
		this.details = details;
	}
	public void run() {
		while(true) {
			try {
				MasterRequest request = details.receiveServerRequest();
				if (request.getOp().equals("lastRequest")) {
					lastRequesthandler(request);
				} else if (request.getOp().equals("extendTail")) {
					extendTailhandler(request);
				} else if (request.getOp().equals("joinedChain"))  {
					joinedChainHandler(request);
				}
			} catch (SocketTimeoutException e) {
				continue;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	private void joinedChainHandler(MasterRequest request) throws IOException {
		// TODO Auto-generated method stub
		int i = details.getBankNames().indexOf(request.getBankName());
		if (details.hasServer(request.getServer().getIp(), request.getServer().getTcpPort(),
				request.getServer().getUdpPort(), i) == -1) {
			request.getServer().setActive(true);
			ServerDef server = new ServerDef(request.getServer().getIpByName(), request.getServer().getTcpPort(),
					request.getServer().getUdpPort());
			server.setActive(true);
			details.addServerToBankList(i, server);
			details.sendClients("changeTail", i, details.getTailIndex(i));
			details.setExtension(i, false);
			details.currentNewTail.set(i, null);
			details.currentRequest.set(i,null);
			details.start[i] = 0;
			details.numTry[i] = 0;
		}
	}
	private void extendTailhandler(MasterRequest request) {
		// TODO Auto-generated method stub
		details.logger.info("Received request from new server to join chain");
		int i = details.getBankNames().indexOf(request.getBankName());
		if (! details.getExtension(i)  &&
				details.hasServer(request.getServer().getIp(), request.getServer().getTcpPort(),
						request.getServer().getUdpPort(), i) == -1 ) {
			details.logger.info("1");
			details.setExtension(i, true);
			details.currentNewTail.set(i, request.getServer());
			details.logger.info("Current request details");
			details.logger.info(details.currentNewTail.get(i).getIpByName()+" "
					+ ""+details.currentNewTail.get(i).getTcpPort()+" "
							+ ""+details.currentNewTail.get(i).getUdpPort());
			details.start[i] = System.currentTimeMillis()/1000L;
			details.numTry[i] = request.getTailtry();
			details.currentRequest.set(i, request);
			details.logger.info(details.start[i]+" "+details.numTry[i]+" "+details.currentRequest.get(i).getOp());
			request.setOp("newTailToJoin");
			details.extendTail(i, request);
		}
	}
	private void lastRequesthandler(MasterRequest request) {
		// TODO Auto-generated method stub
		int i = details.getBankNames().indexOf(request.getBankName());
		int j = 0;
		if ((j = details.hasTcpServer(request.getServer().getIp(), request.getServer().getTcpPort(), i)) != -1) {
			if (j != 0) {
				details.logger.info("Received last request in new successor."
						+ " Forwarding the new predecessor the information about new Successor");
				request.setOp("newPredecessor");
				details.sendServer(request, i, j-1);
			}
		}
	}
}
class PingListner extends Thread{
	private MasterDetails details;
	PingListner(MasterDetails details) {
		this.details = details;
	}
	public void run() {
		while(true) {
			try {
				ClientRequestDetails serverReqDetails = details.receiveUdpRequests();
				Request request = serverReqDetails.request;
				if (request.getOp().equals("ping")) {
					MasterRequest pingRequest = (MasterRequest) request;
					InetAddress serverIp = serverReqDetails.address;
					int serverUdpPort = serverReqDetails.port;
					int i = details.getBankNames().indexOf(pingRequest.getBankName());
					int j = 0;
					if ((j = details.hasUdpServer(serverIp, serverUdpPort, i)) != -1) {
						details.setActive(i,j,true);
					}
				}
			} catch (IOException e) {
				//logger.info("IO Exception");
				continue;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}