package master;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

import common.Request;
import common.Response;
import server.ClientRequestDetails;
import server.MasterRequest;

public class MasterDetails {
	private List<String> bankNames;
	private List<List<ServerDef>> bankList;
	private List<List<ServerDef>> clientList;
	private int hbTimeout; 
	private int newTailTimeout;
	private String masterIp;
	private int masterTcpPort;
	private int masterUdpPort;
	private ServerSocket masterTcpSocket = null;
	private DatagramSocket masterUdpSocket = null;
	private List<AtomicBoolean> extension = null;
	public List<ServerDef> currentNewTail = null;
	public long start[] = null;
	public int numTry[] = null;
	public List<MasterRequest> currentRequest = null;
	public Logger logger;
	public MasterDetails(List<String> bankNames,
			List<List<ServerDef>> bankList, List<List<ServerDef>> clientList,
			int hbTimeout, int newTailTimeout, String masterIp, int masterTcpPort, int masterUdpPort, Logger logger) throws IOException {
		setBankNames(bankNames);
		setBankList(bankList);
		setClientList(clientList);
		setHbTimeout(hbTimeout);
		setNewTailTimeout(newTailTimeout);
		setMasterIp(masterIp);
		setMasterTcpPort(masterTcpPort);
		setMasterTcpSocket(masterTcpPort);
		setMasterUdpPort(masterUdpPort);
		setMasterUdpSocket(masterUdpPort);
		extension = new ArrayList<AtomicBoolean>();
		currentNewTail = new ArrayList<ServerDef>();
		currentRequest = new ArrayList<MasterRequest>();
		start = new long[bankNames.size()];
		numTry = new int[bankNames.size()];
		this.logger = logger;
		int i = 0;
		for (Iterator<String> iter = bankNames.iterator(); iter.hasNext();) {
			iter.next();
			extension.add(new AtomicBoolean(false));
			currentNewTail.add(null);
			currentRequest.add(null);
			start[i] = 0;
			numTry[i] = 0;
			i++;
			
		}
		//setMasterTcpSocket()
		// TODO Auto-generated constructor stub
		
	}
	public List<String> getBankNames() {
		return bankNames;
	}
	public void setBankNames(List<String> bankNames) {
		this.bankNames = bankNames;
	}
	public List<List<ServerDef>> getBankList() {
		return bankList;
	}
	public void setBankList(List<List<ServerDef>> bankList) {
		this.bankList = bankList;
	}
	public void addServerToBankList(int i, ServerDef server) {
		this.bankList.get(i).add(server);
	}
	public List<List<ServerDef>> getClientList() {
		return clientList;
	}
	public void setClientList(List<List<ServerDef>> clientList) {
		this.clientList = clientList;
	}
	public int getHbTimeout() {
		return hbTimeout;
	}
	public void setHbTimeout(int hbTimeout) {
		this.hbTimeout = hbTimeout;
	}
	public int getNewTailTimeout() {
		return newTailTimeout;
	}
	public void setNewTailTimeout(int newTailTimeout) {
		this.newTailTimeout = newTailTimeout;
	}
	public ClientRequestDetails receiveUdpRequests() throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		byte[] requestStream = new byte[4096];
		DatagramPacket requestPacket = new DatagramPacket(requestStream, requestStream.length);
		setMasterUdpSoTimeout(2);
		getMasterUdpSocket().receive(requestPacket);
		ByteArrayInputStream requestArrayStream = new ByteArrayInputStream(requestStream);
		ObjectInputStream inStream = new ObjectInputStream(requestArrayStream);
		Request request = (Request)inStream.readObject();
		return new ClientRequestDetails(request, 
					requestPacket.getAddress(),requestPacket.getPort());
	}
	public int hasUdpServer(InetAddress serverIp, int serverUdpPort, int i) {
		// TODO Auto-generated method stub
		synchronized (bankList.get(i)) {
			for(Iterator<ServerDef> iter = bankList.get(i).iterator(); iter.hasNext();) {
				ServerDef server = iter.next();
				if (server.getIp().equals(serverIp) && server.getUdpPort() == serverUdpPort) {
					return bankList.get(i).indexOf(server);
				}
			}
		}
		return -1;
	}
	public int hasTcpServer(InetAddress serverIp, int serverTcpPort, int i) {
		// TODO Auto-generated method stub
		synchronized (bankList.get(i)) {
			for(Iterator<ServerDef> iter = bankList.get(i).iterator(); iter.hasNext();) {
				ServerDef server = iter.next();
				if (server.getIp().equals(serverIp) && server.getTcpPort() == serverTcpPort) {
					return bankList.get(i).indexOf(server);
				}
			}
		}
		return -1;
	}
	public int hasServer(InetAddress serverIp, int serverTcpPort, int serverUdpPort, int i) {
		if (hasUdpServer(serverIp, serverUdpPort, i) == hasTcpServer(serverIp, serverTcpPort, i)) {
			return hasUdpServer(serverIp, serverUdpPort, i);
		} else
			return -1;
	}
	public String getMasterIp() {
		return masterIp;
	}
	public void setMasterIp(String masterIp) {
		this.masterIp = masterIp;
	}
	public int getMasterTcpPort() {
		return masterTcpPort;
	}
	public void setMasterTcpPort(int masterTcpPort) {
		this.masterTcpPort = masterTcpPort;
	}
	public int getMasterUdpPort() {
		return masterUdpPort;
	}
	public void setMasterUdpPort(int masterUdpPort) {
		this.masterUdpPort = masterUdpPort;
	}
	public ServerSocket getMasterTcpSocket() {
		return masterTcpSocket;
	}
	public void setMasterTcpSocket(int serverTcpPort2) throws IOException {
		this.masterTcpSocket = new ServerSocket(serverTcpPort2);
	}
	public void setMasterTcpSoTimeout(int i) throws SocketException{
		if (this.masterTcpSocket != null) {
			this.masterTcpSocket.setSoTimeout(i*1000);
		}
	}
	public void closeMasterTcpSocket() throws IOException {
		if (this.masterTcpSocket != null) {
			this.masterTcpSocket.close();
		}
	}
	public DatagramSocket getMasterUdpSocket() {
		return masterUdpSocket;
	}
	public void setMasterUdpSocket(int serverUdpPort2) throws SocketException {
		this.masterUdpSocket = new DatagramSocket(serverUdpPort2);
	}
	public void setMasterUdpSoTimeout(int i) throws SocketException {
		if (this.masterUdpSocket != null) {
			this.masterUdpSocket.setSoTimeout(i*1000);
		}
	}
	public void closeMasterUdpSocket() {
		if (this.masterUdpSocket != null)
			this.masterUdpSocket.close();
	}
	public void setActive(int i, int j, boolean b) {
		// TODO Auto-generated method stub
		this.getBankList().get(i).get(j).setActive(true);
	}
	public void printActiveState() {
		// TODO Auto-generated method stub
		for (int i = 0; i < bankNames.size(); i++) {
			synchronized (bankList.get(i)) {
				for(Iterator<ServerDef> iter = bankList.get(i).iterator(); iter.hasNext();) {
					ServerDef server = iter.next();
					if (server.getActive())
						logger.info("True ");
					else
						System.out.println("False ");
				}
				System.out.println("");
			}
		}
	}
	public void setAllActive(boolean b) {
		// TODO Auto-generated method stub
		for (int i = 0; i < bankNames.size(); i++) {
			synchronized (bankList.get(i)) {
				for(Iterator<ServerDef> iter = bankList.get(i).iterator(); iter.hasNext();) {
					iter.next().setActive(b);;
				}
			}
		}
	}
	public int getNumbanks() {
		return bankNames.size();
	}
	public boolean containsActive(int bank, boolean b) {
		// TODO Auto-generated method stub
		synchronized (bankList.get(bank)) {
			for(Iterator<ServerDef> iter = bankList.get(bank).iterator(); iter.hasNext();) {
				if(iter.next().getActive() == b)
					return true;
			}
		}
		return false;
	}
	public void removeServerChain(int bank) {
		// TODO Auto-generated method stub
		bankList.get(bank).clear();
	}
	public void setNewHead(int bank) {
		// TODO Auto-generated method stub
		synchronized (bankList.get(bank)) {
			/*for(Iterator<ServerDef> iter = bankList.get(bank).iterator(); iter.hasNext();) {
				ServerDef server = iter.next();
				if (!server.getActive()) {
					bankList.remove(0);
				} else {
					break;
				}
			}*/
			while (!bankList.get(bank).isEmpty()) {
				if (!bankList.get(bank).get(0).getActive()) {
					bankList.get(bank).remove(0);
				} else {
					break;
				}
			}
		}
	}
	public void sendServer(String op, int bank, int i) {
		// TODO Auto-generated method stub
		MasterRequest request = new MasterRequest(op, this.getBankNames().get(bank));
		InetAddress serverIp = this.bankList.get(bank).get(i).getIp();
		int serverTcpPort = this.bankList.get(bank).get(i).getTcpPort();
		System.out.println("TCP PORT of server = "+serverTcpPort);
		Socket serverSock = null;
		while (true) {
			try {
				serverSock = new Socket(serverIp,serverTcpPort);
				ObjectOutputStream outStream = new ObjectOutputStream(serverSock.getOutputStream());
				outStream.writeObject(request);
				serverSock.close();
				break;
			} catch (ConnectException e) {
				System.out.println("Master not able to connect to server. Looks like its down");
				break;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Sending Failed to server. Retrying to send Server request");
				continue;
			}
		}
		try {
			if (serverSock != null)
				serverSock.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Test");
		}
	}
	public void sendClients(String op, int bank, int i) throws IOException {
		// TODO Auto-generated method stub
		String bankName = bankNames.get(bank);
		String ip = bankList.get(bank).get(i).getIpByName();
		int udpPort = bankList.get(bank).get(i).getUdpPort();
		MasterResponse response = new MasterResponse(op, bankName, ip, udpPort);
		for(Iterator<ServerDef> iter = clientList.get(bank).iterator(); iter.hasNext();) {
			ServerDef client = iter.next();
			DatagramPacket responsePacket = prepareMasterResponsePacket(response, client.getIpByName(), client.getUdpPort());
			getMasterUdpSocket().send(responsePacket);
		}
	}
	public DatagramPacket prepareMasterResponsePacket(MasterResponse response, String clientIp, int clientUdpPort) throws IOException {
		ByteArrayOutputStream responseArrayStream = new ByteArrayOutputStream();
		ObjectOutputStream outStream = new ObjectOutputStream(responseArrayStream);
		outStream.writeObject(response);
		byte[] byteResponse = responseArrayStream.toByteArray();
		byte[] byteResponseStream = new byte[4096];
		System.arraycopy(byteResponse, 0, byteResponseStream, 0, byteResponse.length);
		return new DatagramPacket(byteResponseStream, byteResponseStream.length,
				InetAddress.getByName(clientIp), clientUdpPort);
	}
	public ServerDef getTail(int bank) {
		// TODO Auto-generated method stub
		int index = this.getBankList().get(bank).size()-1;
		if (index < 0)
			return null;
		else
			return this.getBankList().get(bank).get(index);
	}
	public void setNewTail(int bank) {
		// TODO Auto-generated method stub
		synchronized (bankList.get(bank)) {
			while(!this.getBankList().get(bank).isEmpty()) {
				int index = this.getBankList().get(bank).size()-1;
				if (index >=0 ) {
					if (!bankList.get(bank).get(index).getActive()) {
						bankList.get(bank).remove(index);
					} else {
						break;
					}
				}
			}
		}
	}
	public int getTailIndex(int bank) {
		// TODO Auto-generated method stub
		return this.getBankList().get(bank).size()-1;
	}
	public int getIndexActive(int bank, int i, boolean b) {
		// TODO Auto-generated method stub
		int res = -1;
		synchronized (bankList.get(bank)) {
			while(!bankList.get(bank).isEmpty() &&
					i < bankList.get(bank).size()) {
				if(bankList.get(bank).get(i).getActive() == b) {
					res = i;
					break;
				}
				i++;
			}
		}
		return res;
	}
	public void removeFailureNodes(int bank, int index) {
		// TODO Auto-generated method stub 
		synchronized (bankList.get(bank)) {
			while(index < this.getBankList().get(bank).size()) {
				if (bankList.get(bank).get(index).getActive() == false) {
					bankList.get(bank).remove(index);
				} else {
					break;
				}
			}
		}
	}
	public MasterRequest receiveServerRequest() throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		setMasterTcpSoTimeout(2);
		Socket sock = getMasterTcpSocket().accept();
		ObjectInputStream inStream = new ObjectInputStream(sock.getInputStream());
		return (MasterRequest)inStream.readObject();
	}
	public void sendServer(MasterRequest request, int bank, int i) {
		// TODO Auto-generated method stub
		InetAddress serverIp = this.bankList.get(bank).get(i).getIp();
		int serverTcpPort = this.bankList.get(bank).get(i).getTcpPort();
		System.out.println("TCP PORT of server = "+serverTcpPort);
		Socket serverSock = null;
		while (true) {
			try {
				serverSock = new Socket(serverIp,serverTcpPort);
				ObjectOutputStream outStream = new ObjectOutputStream(serverSock.getOutputStream());
				outStream.writeObject(request);
				serverSock.close();
				break;
			} catch (ConnectException e) {
				System.out.println("Master not able to connect to server. Looks like its down");
				break;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Sending Failed to server. Retrying to send Server request");
				continue;
			}
		}
		try {
			if (serverSock != null)
				serverSock.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Test");
		}
	}
	public boolean getExtension(int i) {
		return extension.get(i).get();
	}
	public boolean setExtension(int i, boolean b) {
		if (this.extension != null ) {
			this.extension.get(i).set(b);
		}
		return false;
	}
	public void extendTail(int i, MasterRequest request) {
		// TODO Auto-generated method stub
		if (this.hasServer(currentNewTail.get(i).getIp(), currentNewTail.get(i).getTcpPort(), currentNewTail.get(i).getUdpPort(), i) == -1) {
			if (bankList.get(i).size() != 0) {
				int index =bankList.get(i).size() - 1;
				this.sendServer(request, i, index);
			}
		}
	}
}
