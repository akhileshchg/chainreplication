package server;

import master.ServerDef;
import common.Request;

public class MasterRequest extends Request{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6848562732598294249L;
	private String bankName;
	private int tailtry;
	private ServerDef server;
	private int lastSeq;
	public MasterRequest(String op, String bankName) {
		super(op, "","",0,0);
		setBankName(bankName);
		setTailtry(0);
		setLastSeq(0);
		server = new ServerDef();
	}
	public MasterRequest(String op, String bankName, int tailTry) {
		super(op, "","",0,0);
		setBankName(bankName);
		setTailtry(tailTry);
		setLastSeq(0);
		server = new ServerDef();
	}
	public MasterRequest(String op, String bankName, int lastSeq, ServerDef server) {
		super(op, "","",0,0);
		setBankName(bankName);
		setLastSeq(lastSeq);
		setTailtry(0);
		this.server = server;
	}
	public MasterRequest(String op, String bankName, int lastSeq, ServerDef server, int tailTry) {
		super(op, "","",0,0);
		setBankName(bankName);
		setLastSeq(lastSeq);
		setTailtry(tailTry);
		this.server = server;
	}
	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public int getTailtry() {
		return tailtry;
	}

	public void setTailtry(int tailtry) {
		this.tailtry = tailtry;
	}

	public ServerDef getServer() {
		return server;
	}

	public void setServer(ServerDef server) {
		this.server = server;
	}

	public int getLastSeq() {
		return lastSeq;
	}

	public void setLastSeq(int lastSeq) {
		this.lastSeq = lastSeq;
	}
}
