package server;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import common.*;

public class Account implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2583849814796783905L;
	private String accNum;
	private int balance;
	private List<ProcessedRequest> history;
	public Account(String accNum){
		this.setAccNum(accNum);
		this.setBalance(0);
		history = new ArrayList<ProcessedRequest>();
	}
	public void addBalance(int amount) {
		// TODO Auto-generated method stub
		balance+=amount;
	}
	public void subBalance(int amount) {
		balance-=amount;
	}
	public void addProcessedRequest(ProcessedRequest pRequest){
		history.add(pRequest);
	}
	public int checkConsistency(Request request){
		return 0;
	}
	/**
	 * @return the accNum
	 */
	public String getAccNum() {
		return accNum;
	}
	/**
	 * @param accNum the accNum to set
	 */
	public void setAccNum(String accNum) {
		this.accNum = accNum;
	}
	/**
	 * @return the balance
	 */
	public int getBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(int balance) {
		this.balance = balance;
	}
	public  List<ProcessedRequest> getHistory(){
		return history;
	}
}