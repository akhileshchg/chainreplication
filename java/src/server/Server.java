package server;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;

import master.ServerDef;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import org.ini4j.*;

import common.Request;
import common.SyncRequest;

public class Server {
	public final static Logger logger = Logger.getLogger("Server log"); 
	public static void main(String[] args) {
		if (args.length != 3){
			throw new ExceptionInInitializerError("Provide, config file"
					+ " followed by bank number and server number");
		}
		try {
			FileHandler fh = new FileHandler("server"+"_"+args[1]+"_"+args[2]+"_"+Long.toString(System.currentTimeMillis()/1000L));  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  
	        logger.setUseParentHandlers(false);
	        logger.info("In Master - main method");
		} catch (SecurityException e) {  
			logger.info("Problem with creating log file");
	        e.printStackTrace(); 
	        System.exit(1);
	    	} catch (IOException e) {
	    	logger.info("IO Exception");
			e.printStackTrace();
			System.exit(1);
		}
		Ini configHandle = null;
		try {
			configHandle = new Ini(new File(args[0]));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			logger.info("Problem with config file");
			e1.printStackTrace();
			System.exit(1);
		}
		String currentBank = args[1];
		String currentServer = args[2];
		String masterIp = configHandle.get("GENERIC_BANK_DETAILS", "MASTER_IP");
		int masterUdpPort = Integer.parseInt(configHandle.get("GENERIC_BANK_DETAILS", "MASTER_UDP_PORT"));
		int masterTcpPort = Integer.parseInt(configHandle.get("GENERIC_BANK_DETAILS", "MASTER_TCP_PORT"));
		String bankName = configHandle.get("BANK_"+currentBank, "BANK_NAME");
		String serverId = "server-"+bankName+"-"+currentServer;
		int chainLen = Integer.parseInt(configHandle.get("BANK_"+currentBank, "CHAIN_LEN"));
		String serverIp = configHandle.get("BANK_"+currentBank, "IP_"+currentServer);
		int serverUdpPort = Integer.parseInt(configHandle.get("BANK_"+currentBank, "SERVER_UDP_PORT_"+currentServer));
		logger.info(configHandle.get("BANK_"+currentBank, "SERVER_UDP_PORT_"+currentServer)+" "+serverUdpPort);
		int serverTcpPort = Integer.parseInt(configHandle.get("BANK_"+currentBank, "SERVER_TCP_PORT_"+currentServer));
		String lifetimeType = configHandle.get("BANK_"+currentBank, "LIFETIME_TYPE_"+currentServer);
		int lifetime = Integer.parseInt(configHandle.get("BANK_"+currentBank, "LIFETIME_"+currentServer));
		int startupDelay = Integer.parseInt(configHandle.get("BANK_"+currentBank, "STARTUP_DELAY_"+currentServer))*1000;
		int failPred = 0;
		try {
			failPred = Integer.parseInt(configHandle.get("BANK_"+currentBank, "FAIL_ON_NEWPRED_"+currentServer));
		} catch (NumberFormatException e) {
			failPred = 0;
		}
		int failSucc = 0;
		try {
			failSucc = Integer.parseInt(configHandle.get("BANK_"+currentBank, "FAIL_NEWSUCC_"+currentServer));
		} catch (NumberFormatException e) {
			failSucc = 0;
		}
		int failExt = 0;
		try {
			failExt = Integer.parseInt(configHandle.get("BANK_"+currentBank, "FAIL_ON_CHAINEXT_"+currentServer));
		} catch (NumberFormatException e) {
			failExt = 0;
		}
		int failCurr = 0;
		try {
			failCurr = Integer.parseInt(configHandle.get("BANK_"+currentBank, "FAIL_CURR_CHAINEXT_"+currentServer));
		} catch (NumberFormatException e) {
			failCurr = 0;
		}
		int joinRetries = Integer.parseInt(configHandle.get("BANK_"+currentBank, "NUM_JOIN_RETRIES_"+currentServer));
		int joinTimeout = Integer.parseInt(configHandle.get("BANK_"+currentBank, "NUM_JOIN_TIMEOUT_"+currentServer));
		logger.info(failPred+" "+failSucc+" "+failExt+" "+joinRetries+" "+joinTimeout);
		boolean head = false;
		boolean tail = false;
		String predecessorIp = "";
		int predecessorTcpPort = 0;
		int predecessorUdpPort = 0;
		String successorIp = "";
		int successorTcpPort = 0;
		int successorUdpPort = 0;
		int currServer = Integer.parseInt(currentServer);
		if (currServer <= chainLen) {
			if (currServer == 1)
				head = true;
			else{
				predecessorIp = configHandle.get("BANK_"+currentBank, "IP_"+Integer.toString(currServer-1));
				predecessorTcpPort = Integer.parseInt(configHandle.get("BANK_"+currentBank, 
									"SERVER_TCP_PORT_"+Integer.toString(currServer-1)));
				predecessorUdpPort = Integer.parseInt(configHandle.get("BANK_"+currentBank, 
						"SERVER_UDP_PORT_"+Integer.toString(currServer-1)));
			}
			if (currServer == chainLen)
				tail = true;
			else{
				successorIp = configHandle.get("BANK_"+currentBank, "IP_"+Integer.toString(currServer+1));
				successorTcpPort = Integer.parseInt(configHandle.get("BANK_"+currentBank, 
									"SERVER_TCP_PORT_"+Integer.toString(currServer+1)));
				successorUdpPort = Integer.parseInt(configHandle.get("BANK_"+currentBank, 
						"SERVER_UDP_PORT_"+Integer.toString(currServer+1)));
			}
		}
		logger.info("Server = "+serverId+" Status:");
		if (head)
			logger.info("Head");
		if (tail)
			logger.info("Tail");
		logger.info("Startup delay = "+startupDelay);
		logger.info("Lifetime type = "+lifetimeType);
		logger.info("Lifetime = "+lifetime);
	
			Details details = null;
			try {
				details = new Details(serverId, bankName, startupDelay, lifetimeType,
										lifetime, predecessorIp, predecessorTcpPort, predecessorUdpPort,
										successorIp, successorTcpPort, successorUdpPort, 
										serverIp, serverTcpPort, serverUdpPort, head, tail,
										masterIp, masterTcpPort, masterUdpPort,
										failPred, failSucc, failExt, joinRetries, joinTimeout, failCurr, logger);
				PingThread ping = new PingThread(details);
				ping.start();
				Thread.sleep(details.getStartupDelay());
				if (details.getSuccessorIp().equals("") && details.getSuccessorTcpPort() == 0 && details.getSuccessorUdpPort() == 0
						&& details.getPredecessorIp().equals("") && details.getPredecessorTcpPort() == 0 && details.getPredecessorUdpPort() == 0) {
					AddToChain addChain = new AddToChain(details);
					addChain.start();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.info("Error before setting up all the sockets");
				e.printStackTrace();
				System.exit(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				logger.info("Error while sleeping for start up delay");
				e.printStackTrace();
				System.exit(1);
			}
			if (details.getLifetimeType().equals("time")) {
				TimerThread timer = new TimerThread(details);
				timer.start();
			}
			ClientListner cListner = new ClientListner(details);
			cListner.start();
			while(details.isMainStop()) {
				try {
					Request request = details.serverReceiveRequest();
					if (request.getOp().equals("sync")) {
						syncHandler(details, request);
						if (details.getLife() == details.getLifetime())
							break;
					} else if (request.getOp().equals("sync_ack")) {
						sentAckHandler(details,request);
					} else if (request.getOp().equals("newHead")) {
						newHeadHandler(details, request);
					} else if (request.getOp().equals("newTail")) {
						newTailHandler(details,request);
					} else if (request.getOp().equals("newSuccessor")) {
						newSuccHandler(details,request);
					} else if (request.getOp().equals("newPredecessor")) {
						newPredHandler(details,request);
					} else if (request.getOp().equals("allSet")) {
						allSetHandler(details,request);
					} else if (request.getOp().equals("killNewSuccessor")) {
						killNewSuccHandler(details,request);
					} else if (request.getOp().equals("newTailToJoin")) {
						newTailToJoinHandler(details,request);
					} else if (request.getOp().equals("currentHistory")) {
						currentHistoryHandler(details, request);
					} else if (request.getOp().equals("endOfSent")) {
						endOfSenthandler(details,request);
					}
				} catch (ClassNotFoundException e) {
					logger.info("Class not found error while TCP receive");
					e.printStackTrace();
					System.exit(1);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					continue;
					//e.printStackTrace();
					//System.exit(1);
				}
			}
			
			//try {
				details.setThreadStop(false);
				/*if (!details.getServerUdpSocket().isClosed()) {
					details.setServerUdpSoTimeout(0);
					details.closeServerUdpSocket();
				}
				if (!details.getServerTcpSocket().isClosed()) {
					details.setServerTcpSoTimeout(0);
					details.closeServerTcpSocket();
				}*/
				details.printDetails();
				details.printSent();
				System.exit(0);
			/*} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			
	}
	private static void endOfSenthandler(Details details, Request req) throws IOException {
		// TODO Auto-generated method stub
		if (!details.isTail()) {
			MasterRequest request = (MasterRequest)req;
			if (details.getFailExt() == 1) {
				logger.info("New server failed during chain Extension");
				System.exit(0);
			}
			details.setPredecessorIp(request.getServer().getIpByName());
			details.setPredecessorTcpPort(request.getServer().getTcpPort());
			details.setPredecessorUdpPort(request.getServer().getUdpPort());
			details.setTail(true);
			logger.info("New server is the new tail");
			logger.info("New server informing master about completion of joining process");
			request = new MasterRequest("joinedChain", details.getBankName(),0,
					new ServerDef(details.getServerIp(), details.getServerTcpPort(), details.getServerUdpPort()));
			details.sendMaster(request);
		}
	}
	private static void currentHistoryHandler(Details details, Request req) {
		// TODO Auto-generated method stub
		if (!details.isTail()) {
			HistoryRequest request = (HistoryRequest) req;
			details.setAccountList(request.getAccountList());
			details.clearSent();
			details.setReqSequence(0);
		}
	}
	
	private static void newTailToJoinHandler(Details details, Request req) throws UnknownHostException, IOException {
		// TODO Auto-generated method stub
		if (details.isTail()) {
			MasterRequest request = (MasterRequest) req;
			logger.info("Received new server to join request from master");
			details.transition.set(true);
			details.setSuccessorIp(request.getServer().getIpByName());
			details.setSuccessorTcpPort(request.getServer().getTcpPort());
			details.setSuccessorUdpPort(request.getServer().getUdpPort());
			logger.info(details.getSuccessorTcpPort()+" "+details.getSuccessorUdpPort());
			HistoryRequest hreq = new HistoryRequest("currentHistory", details.getBankName(), details.getAccountList());
			try {
				details.sendSuccessorRequest(hreq);
			} catch (IOException e) {
				e.printStackTrace();
			}
			logger.info("Completed Transfering Account History");
			details.setStopSync(true);
			details.transmitMissedRequests(0);
			if (details.getFailCurr() == 1) {
				logger.info("Current tail server died during chain extension");
				System.exit(0);
			}
			logger.info("Completed transfering requests in Sent queue");
			
			MasterRequest endRequest = new MasterRequest("endOfSent", request.getBankName(), 0,
					new ServerDef(details.getServerIp(), details.getServerTcpPort(), details.getServerUdpPort()), request.getTailtry());
			details.sendSuccessorRequest(endRequest);
			details.setTail(false);
			details.setStopSync(false);
			details.transition.set(false);
			logger.info("Current tail is no more a tail");
		}
	}
	private static void killNewSuccHandler(Details details, Request request) {
		// TODO Auto-generated method stub
		logger.info("The new successor server died during chain reconcilation");
		System.exit(0);
	}
	private static void allSetHandler(Details details, Request req) {
		// TODO Auto-generated method stub
		logger.info("Setting predecessor of failed node as"
				+ "current node's predecessor");
		MasterRequest request = (MasterRequest)req;
		details.setPredecessorIp(request.getServer().getIpByName());
		details.setPredecessorTcpPort(request.getServer().getTcpPort());
		details.setPredecessorUdpPort(request.getServer().getUdpPort());
	}
	private static void newPredHandler(Details details, Request req) throws UnknownHostException, IOException {
		MasterRequest request  = (MasterRequest) req;
		// TODO Auto-generated method stub
		logger.info("New Predecessor Request from master is received");
		if (details.getFailPred() == 1) {
			logger.info("Predecessor Failed during reconcilation");
			System.exit(1);
		}
		details.setStopSync(true);
		details.setSuccessorIp(request.getServer().getIpByName());
		details.setSuccessorTcpPort(request.getServer().getTcpPort());
		details.setSuccessorUdpPort(request.getServer().getUdpPort());
		int index = details.getSentIndex(request.getLastSeq()+1);
		if (index >=0 ) {
			details.transmitMissedRequests(index);
			logger.info("Transmission of missed Requests completed");
		}else
			logger.info("No missed requests");
		if (details.getKilledNewSuccessor() < details.getFailSucc()) {
			Request killNewSucc = new MasterRequest("killNewSuccessor", details.getBankName());
			details.sendSuccessorRequest(killNewSucc);
			details.incrKilledNewSuccessor();
			details.setStopSync(false);
		} else {
			details.setStopSync(false);
			logger.info("All Done for new predecessor. Setting failed node's"
					+ "successor as new successor");
			Request allSet = new MasterRequest("allSet", details.getBankName(),0,
										new ServerDef(details.getServerIp(), details.getServerTcpPort(), details.getServerUdpPort()));
			details.sendSuccessorRequest(allSet);
		}
	}
	private static void newSuccHandler(Details details, Request request) throws UnknownHostException, IOException {
		// TODO Auto-generated method stub
		logger.info("New Successor request received from master");
		MasterRequest masterReq = new MasterRequest("lastRequest", details.getBankName(), details.getReqSequence(),
										new ServerDef(details.getServerIp(), details.getServerTcpPort(), details.getServerUdpPort()));
		details.sendMaster(masterReq);
	}
	private static void newTailHandler(Details details, Request request) {
		// TODO Auto-generated method stub
		logger.info("Server "+details.getServerId()+""
				+ "is now tail");
		details.setTail(true);
		details.setSuccessorIp("");
		details.setSuccessorTcpPort(0);
		details.setSuccessorUdpPort(0);
		details.clearSent();
	}
	private static void newHeadHandler(Details details, Request request) {
		// TODO Auto-generated method stub
		logger.info("Server "+details.getServerId()+""
				+ "is now Head");
		details.setHead(true);
		details.setPredecessorIp("");
		details.setPredecessorTcpPort(0);
		details.setPredecessorUdpPort(0);
	}
	private static void sentAckHandler(Details details, Request request) throws UnknownHostException, IOException {
		// TODO Auto-generated method stub
		SyncRequest syncRequest = (SyncRequest) request;
		logger.info("Received Sent Ack for request "+syncRequest.getServerSequence());
		int index = 0;
		if ((index = details.sentContainsServerSequence(syncRequest)) != -1) {
			details.removeAllBefore(index);
			if (!details.isHead()) {
				details.sendPredecessorRequest(syncRequest);
			}
		}
	}
	static void syncHandler(Details details, Request request) throws IOException {
		if (details.getLifetimeType().equals("receive") && !details.isHead())
			details.incrementLife();
		SyncRequest syncRequest = (SyncRequest) request;
		logger.info("Server "+details.getServerId()+" received"
				+ "sync message from predecessor "+syncRequest.getServerSequence());
		if (!details.isHead() && !syncRequest.getMainOp().equals("query")) {
			if (syncRequest.getServerSequence() <= details.getReqSequence()) {
				return;
			} else {
				details.setReqSequence(syncRequest.getServerSequence());
			}
		}
		/*if (!details.isHead() && !syncRequest.getMainOp().equals("query")) {
			
		}*/
		int index = details.getIndex(syncRequest.getAccount());
		int check = details.consistencyCheck(index, syncRequest.getRequest());
		details.setBalance(index, syncRequest.getBalance());
		if (check == 0 && !syncRequest.getMainOp().equals("query") &&
				syncRequest.getStatus().equals("Processed")) {
			ProcessedRequest processedRequest = new ProcessedRequest(syncRequest.getRequest());
			logger.info("Sync message valid. Adding to processedHistory of accounts");
			details.addProcessedRequest(index, processedRequest);
		}
		logger.info("Server "+details.getServerId()+" forwarding sync message "+syncRequest.getServerSequence()
				+ " to successor");
		details.sync(syncRequest);
		if (details.getLifetimeType().equals("send") && !details.isHead())
			details.incrementLife();
	}
}

class ClientListner extends Thread{
	public Details details;
	ClientListner(Details details) {
		this.details = details;
	}
	public void run(){
		while (details.isThreadStop()){
			try {
				ClientRequestDetails clientReqDetails = details.clientReceiveRequestPacket();
				if (details.getLifetimeType().equals("receive") && 
						(details.isHead() || details.isTail())) {
					details.incrementLife();
				}
				Request request = clientReqDetails.request;
				details.logger.info("Server "+details.getServerId()+"Received request with RequestId "+request.getRequestID()+
						" ,Operation "+request.getOp()+" Account "+request.getAccount()+
						" Amount = "+request.getAmount());
				String status = "Processed";
				int index = details.getIndex(request.getAccount());
				if (details.isHead()){
					if (!request.getOp().equals("query")){
						int check = details.consistencyCheck(index, request);
						if (check == -1){
							status = "InconsistentWithHistory";
						}
						else if (check == 0){
							if (request.getOp().equals("deposit")) {
								details.addBalance(index, request.getAmount());
							}
							if (request.getOp().equals("withdraw")) {
								if (details.getBalance(index) >= 
										request.getAmount()) {
									details.subBalance(index,request.getAmount());
								} else{
									status = "InSufficientFunds";
								}
							}
							if (status.equals("Processed")) {
								ProcessedRequest processedRequest = new ProcessedRequest(request);
								details.addProcessedRequest(index, processedRequest);
							}
						}
						SyncRequest syncRequest = new SyncRequest(request, status, 
								details.getBalance(index), 
								clientReqDetails.address, 
								clientReqDetails.port, details.incrReqSequence());
						details.logger.info("Synching message to successor");
						details.sync(syncRequest);
						if (details.getLifetimeType().equals("send") && 
								(details.isHead() || details.isTail())) {
							details.incrementLife();
						}
					}
				}
				if (details.isTail()) {
					if (request.getOp().equals("query")) {
						details.logger.info("Tail "+details.getServerId()+" received query request"
								+ "with id "+request.getRequestID()+" for account"+request.getAccount());
						SyncRequest syncRequest = new SyncRequest(request, status, 
								details.getBalance(index), 
								clientReqDetails.address, 
								clientReqDetails.port, 0);
						details.logger.info("Tail sending response to client for query with id "+request.getRequestID());
						details.sync(syncRequest);
						if (details.getLifetimeType().equals("send") && 
								(details.isHead() || details.isTail())) {
							details.incrementLife();
						}
					}
				}
				if (details.getLife() == details.getLifetime())
					break;
			} catch (IOException e) {
				//logger.info("IO Exception");
				continue;
				//e.printStackTrace();
				//System.exit(1);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				System.err.println("Class not found exception on TCP receive");
				e.printStackTrace();
				System.exit(1);
			}
		}
		try {
			details.setMainStop(false);
			if (!details.getServerUdpSocket().isClosed()) {
				details.setServerUdpSoTimeout(0);
				details.closeServerUdpSocket();
			}
			if (!details.getServerTcpSocket().isClosed()) {
				details.setServerTcpSoTimeout(0);
				details.closeServerTcpSocket();
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
class TimerThread extends Thread {
	public Details details;
	TimerThread(Details details) {
		this.details = details;
	}
	public void run() {
		try {
			Thread.sleep(details.getLifetime()*1000);
			details.logger.info("Server lifetime over. Server dying");
			details.printDetails();
			details.printSent();
			System.exit(0);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
class PingThread extends Thread {
	public Details details;
	PingThread(Details details) {
		this.details = details;
	}
	public void run() {
		while (true) {
			try {
				details.pingMaster();
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
class AddToChain extends Thread {
	public Details details;
	
	AddToChain(Details details) {
		this.details = details;
	}
	public void run() {
		boolean join = false;
		for(int tailTry = 0; tailTry < details.getJoinRetries(); tailTry++) {
			details.logger.info("Server "+details.getServerId()+" requesting master to join chain");
			try {
				MasterRequest request = new MasterRequest("extendTail", details.getBankName(),0,
										new ServerDef(details.getServerIp(), details.getServerTcpPort(), details.getServerUdpPort()),
										tailTry);
				details.sendMaster(request);
				long unixTime = System.currentTimeMillis() / 1000L;
				while ((System.currentTimeMillis()/1000L) - unixTime <= details.getJointTimeout()) {
					if (!details.isTail()) {
						
						//details.setJoined(false);
						join = true;
						break;
					}		
				}
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				details.logger.info("IOException while joining chain");
			}
			if (join)
				break;
		}
	}
}