package server;

import java.util.List;

import common.Request;

public class HistoryRequest extends Request{
	private String bankName;
	private List<Account> accountList;
	public HistoryRequest(String op, String bankName,
			List<Account> accountList) {
		// TODO Auto-generated constructor stub
		setOp(op);
		setBankName(bankName);
		setAccountList(accountList);
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public List<Account> getAccountList() {
		return accountList;
	}

	public void setAccountList(List<Account> accountList) {
		this.accountList = accountList;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6871766068910584078L;
	
}
