package server;
import java.io.Serializable;

import common.*;

class ProcessedRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5557652990960430400L;
	private String op;
	private String requestID;
	private String account;
	private int amount;
	public ProcessedRequest(Request request){
		op = request.getOp();
		requestID = request.getRequestID();
		account = request.getAccount();
		amount = request.getAmount();
	}
	public boolean contains(Request request) {
		// TODO Auto-generated method stub
		return false;
	}
	/**
	 * @return the op
	 */
	public String getOp() {
		return op;
	}
	/**
	 * @param op the op to set
	 */
	public void setOp(String op) {
		this.op = op;
	}
	/**
	 * @return the requestID
	 */
	public String getRequestID() {
		return requestID;
	}
	/**
	 * @param requestID the requestID to set
	 */
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}
	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}
	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}
}