package server;

import java.net.InetAddress;

import common.Request;

public class ClientRequestDetails {
	public Request request;
	public InetAddress address;
	public int port;
	public ClientRequestDetails(Request request2, InetAddress address, int port) {
		// TODO Auto-generated constructor stub
		request = request2;
		this.address = address;
		this.port = port;
	}
}
