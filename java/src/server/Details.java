package server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import common.Request;
import common.Response;
import common.SyncRequest;

public class Details {
	private String serverId;
	private String bankName;
	private int startupDelay;
	private int lifetime;
	private String lifetimeType;
	
	private String predecessorIp;
	private int predecessorTcpPort;
	private int predecessorUdpPort;
	//private ServerSocket predecessorTcpSocket;
	//private DatagramSocket predecessorUdpSocket;
	
	private String successorIp;
	private int successorTcpPort;
	private int successorUdpPort;
	//private ServerSocket successorTcpSocket;
	//private DatagramSocket successorUdpSocket;
	
	private String serverIp;
	private int serverTcpPort;
	private int serverUdpPort;
	private ServerSocket serverTcpSocket = null;
	private DatagramSocket serverUdpSocket = null;
	
	private String masterIp;
	private int masterTcpPort;
	private int masterUdpPort;
	
	private AtomicBoolean head;
	private AtomicBoolean tail;
	private AtomicBoolean mainStop;
	private AtomicBoolean threadStop;
	private AtomicInteger reqSequence = null;
	private AtomicInteger life = null;
	private List<Account> accountList =  Collections.synchronizedList(new ArrayList<Account>());
	private List<SyncRequest> Sent = Collections.synchronizedList(new ArrayList<SyncRequest>());
	private AtomicBoolean stopSync = null;
	private int failPred;
	private int failSucc;
	private int failExt;
	private int failCurr;
	private int joinRetries;
	private int jointTimeout;
	private int killedNewSuccessor;
	AtomicBoolean transition = null;
	public Logger logger;
	
	Details(String serverId, String bankName, int startupDelay, String lifetimeType, int lifetime,
			String predecessorIp, int predecessorTcpPort, int predecessorUdpPort,
			String successorIp, int successorTcpPort, int successorUdpPort,
			String serverIp, int serverTcpPort, int serverUdpPort,
			boolean head, boolean tail, String masterIp, int masterTcpPort, int masterUdpPort,
			int failPred, int failSucc, int failExt, int joinRetries, int joinTimeout, int failCurr, Logger logger) throws IOException {
		setServerId(serverId);
		setBankName(bankName);
		setStartupDelay(startupDelay);
		setLifetimeType(lifetimeType);
		setLifetime(lifetime);
		
		setSuccessorIp(successorIp);
		setSuccessorTcpPort(successorTcpPort);
		setSuccessorUdpPort(successorUdpPort);
		
		setPredecessorIp(predecessorIp);
		setPredecessorTcpPort(predecessorTcpPort);
		setPredecessorUdpPort(predecessorUdpPort);
		
		setServerIp(serverIp);
		setServerTcpPort(serverTcpPort);
		setServerUdpPort(serverUdpPort);
		setServerTcpSocket(serverTcpPort);
		setServerTcpSoTimeout(1);
		setServerUdpSocket(serverUdpPort);
		setServerUdpSoTimeout(1);
		setHead(head);
		setTail(tail);
		
		setMasterIp(masterIp);
		setMasterTcpPort(masterTcpPort);
		setMasterUdpPort(masterUdpPort);
		
		setMainStop(true);
		setThreadStop(true);
		setReqSequence(0);
		setLife(0);
		setStopSync(false);
		setFailExt(failExt);
		setFailPred(failPred);
		setFailSucc(failSucc);
		setFailCurr(failCurr);
		setJoinRetries(joinRetries);
		setJointTimeout(joinTimeout);
		killedNewSuccessor = 0;
		transition = new AtomicBoolean(false);
		this.logger = logger;
	}

	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 * @return the startupDelay
	 */
	public int getStartupDelay() {
		return startupDelay;
	}

	/**
	 * @param startupDelay the startupDelay to set
	 */
	public void setStartupDelay(int startupDelay) {
		this.startupDelay = startupDelay;
	}

	public int getLifetime() {
		return lifetime;
	}

	public void setLifetime(int lifetime) {
		this.lifetime = lifetime;
	}

	public String getLifetimeType() {
		return lifetimeType;
	}

	public void setLifetimeType(String lifetimeType) {
		this.lifetimeType = lifetimeType;
	}

	public String getPredecessorIp() {
		return predecessorIp;
	}

	public void setPredecessorIp(String predecessorIp) {
		this.predecessorIp = predecessorIp;
	}

	public int getPredecessorTcpPort() {
		return predecessorTcpPort;
	}

	public void setPredecessorTcpPort(int predecessorTcpPort) {
		this.predecessorTcpPort = predecessorTcpPort;
	}

	public int getPredecessorUdpPort() {
		return predecessorUdpPort;
	}

	public void setPredecessorUdpPort(int predecessorUdpPort) {
		this.predecessorUdpPort = predecessorUdpPort;
	}
/*
	public ServerSocket getPredecessorTcpSocket() {
		return predecessorTcpSocket;
	}

	public void setPredecessorTcpSocket(ServerSocket predecessorTcpSocket) {
		this.predecessorTcpSocket = predecessorTcpSocket;
	}

	public DatagramSocket getPredecessorUdpSocket() {
		return predecessorUdpSocket;
	}

	public void setPredecessorUdpSocket(DatagramSocket predecessorUdpSocket) {
		this.predecessorUdpSocket = predecessorUdpSocket;
	}
*/
	public String getSuccessorIp() {
		return successorIp;
	}

	public void setSuccessorIp(String successorIp) {
		this.successorIp = successorIp;
	}

	public int getSuccessorTcpPort() {
		return successorTcpPort;
	}

	public void setSuccessorTcpPort(int successorTcpPort) {
		this.successorTcpPort = successorTcpPort;
	}

	public int getSuccessorUdpPort() {
		return successorUdpPort;
	}

	public void setSuccessorUdpPort(int successorUdpPort) {
		this.successorUdpPort = successorUdpPort;
	}
/*
	public ServerSocket getSuccessorTcpSocket() {
		return successorTcpSocket;
	}

	public void setSuccessorTcpSocket(ServerSocket successorTcpSocket) {
		this.successorTcpSocket = successorTcpSocket;
	}

	public DatagramSocket getSuccessorUdpSocket() {
		return successorUdpSocket;
	}

	public void setSuccessorUdpSocket(DatagramSocket successorUdpSocket) {
		this.successorUdpSocket = successorUdpSocket;
	}
*/
	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public int getServerTcpPort() {
		return serverTcpPort;
	}

	public void setServerTcpPort(int serverTcpPort) {
		this.serverTcpPort = serverTcpPort;
	}

	public int getServerUdpPort() {
		return serverUdpPort;
	}

	public void setServerUdpPort(int serverUdpPort) {
		this.serverUdpPort = serverUdpPort;
	}

	public ServerSocket getServerTcpSocket() {
		return serverTcpSocket;
	}

	public void setServerTcpSocket(int serverTcpPort2) throws IOException {
		this.serverTcpSocket = new ServerSocket(serverTcpPort2);
	}
	public void setServerTcpSoTimeout(int i) throws SocketException{
		if (this.serverTcpSocket != null) {
			this.serverTcpSocket.setSoTimeout(i*1000);
		}
	}
	public void closeServerTcpSocket() throws IOException {
		if (this.serverTcpSocket != null) {
			this.serverTcpSocket.close();
		}
	}
	public DatagramSocket getServerUdpSocket() {
		return serverUdpSocket;
	}

	public void setServerUdpSocket(int serverUdpPort2) throws SocketException {
		this.serverUdpSocket = new DatagramSocket(serverUdpPort2);
	}
	public void setServerUdpSoTimeout(int i) throws SocketException {
		if (this.serverUdpSocket != null) {
			this.serverUdpSocket.setSoTimeout(i*1000);
		}
	}
	public void closeServerUdpSocket() {
		if (this.serverUdpSocket != null)
			this.serverUdpSocket.close();
	}
	public boolean isHead() {
		return head.get();
	}

	public void setHead(boolean head) {
		this.head = new AtomicBoolean(head);
	}

	public boolean isTail() {
		return tail.get();
	}

	public void setTail(boolean tail) {
		this.tail = new AtomicBoolean(tail);
	}
	public boolean isThreadStop() {
		return this.threadStop.get();
	}
	public void setThreadStop(boolean threadStop) {
		this.threadStop = new AtomicBoolean(threadStop);
	}
	public boolean isMainStop() {
		return mainStop.get();
	}
	public void setMainStop(boolean mainStop) {
		this.mainStop = new AtomicBoolean(mainStop);
	}
	public List<Account> getAccountList() {
		return accountList;
	}
	public void setAccountList(List<Account> accountList) {
		this.accountList = accountList;
	}
	public int getReqSequence() {
		return reqSequence.get();
	}

	public void setReqSequence(int s) {
		if (this.reqSequence == null)
			this.reqSequence = new AtomicInteger(s);
		else
			this.reqSequence.set(s);
	}
	public void setLife(int s) {
		if (this.life == null)
			this.life = new AtomicInteger(s);
		else
			this.life.set(s);
	}
	public int getLife() {
		return life.get();
	}
	public int incrementLife() {
		if (this.life != null)
			return this.life.incrementAndGet();
		return 0;
	}
	public ClientRequestDetails clientReceiveRequestPacket() throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		byte[] requestStream = new byte[4096];
		DatagramPacket requestPacket = new DatagramPacket(requestStream, requestStream.length);
		setServerUdpSoTimeout(60);
		getServerUdpSocket().receive(requestPacket);
		ByteArrayInputStream requestArrayStream = new ByteArrayInputStream(requestStream);
		ObjectInputStream inStream = new ObjectInputStream(requestArrayStream);
		Request request = (Request)inStream.readObject();
		return new ClientRequestDetails(request, 
					requestPacket.getAddress(),requestPacket.getPort());
			
	}
	public DatagramPacket prepareClientResponsePacket(SyncRequest syncRequest) throws IOException {
		Response response = new Response(syncRequest);
		ByteArrayOutputStream responseArrayStream = new ByteArrayOutputStream();
		ObjectOutputStream outStream = new ObjectOutputStream(responseArrayStream);
		outStream.writeObject(response);
		byte[] byteResponse = responseArrayStream.toByteArray();
		byte[] byteResponseStream = new byte[4096];
		System.arraycopy(byteResponse, 0, byteResponseStream, 0, byteResponse.length);
		return new DatagramPacket(byteResponseStream, byteResponseStream.length,
				syncRequest.getClientInetAddress(), syncRequest.getClientPort());
	}
	public int consistencyCheck(int index, Request request) {
		// TODO Auto-generated method stub
		synchronized (accountList) {		
			Account account = accountList.get(index);
			for(Iterator<ProcessedRequest> iter = account.getHistory().iterator(); iter.hasNext();){
				ProcessedRequest processedRequest = iter.next();
				if (processedRequest.getRequestID().equals(request.getRequestID())){
					if (processedRequest.getAmount() == request.getAmount() && processedRequest.getOp().equals(request.getOp()))
						return 1;
					else
						return -1;
				}
			}
			
			return 0;
		}
	}

	public int getIndex(String accNum) {
		// TODO Auto-generated method stub
		synchronized(accountList) {
			for(Iterator<Account> iter = accountList.iterator(); iter.hasNext();){
				Account account = iter.next();
				if (accNum.equals(account.getAccNum()))
					return accountList.indexOf(account);
			}
			Account account = new Account(accNum);
			accountList.add(account);
			return accountList.indexOf(account);
		}
	}

	public void addBalance(int index, int amount) {
		// TODO Auto-generated method stub
		synchronized(accountList) {
			accountList.get(index).addBalance(amount);
		}
	}

	public int getBalance(int index) {
		// TODO Auto-generated method stub
		synchronized(accountList) {
			return accountList.get(index).getBalance();
		}
	}

	public void subBalance(int index, int amount) {
		// TODO Auto-generated method stub
		synchronized(accountList) {
			accountList.get(index).subBalance(amount);
		}
	}

	/*public Request getRequest(DatagramPacket requestPacket) throws IOException, ClassNotFoundException {
		byte[] requestStream = new byte[4096];
		// TODO Auto-generated method stub
		ByteArrayInputStream requestArrayStream = new ByteArrayInputStream(requestStream );
		ObjectInputStream inStream = new ObjectInputStream(requestArrayStream);
		return (Request)inStream.readObject();
	}*/

	public InetAddress getClientAddr(DatagramPacket requestPacket) {
		// TODO Auto-generated method stub
		return requestPacket.getAddress();
	}

	public int getClientPort(DatagramPacket requestPacket) {
		// TODO Auto-generated method stub
		return requestPacket.getPort();
	}

	public void sync(SyncRequest syncRequest) throws IOException {
		// TODO Auto-generated method stub
		if (tail.get()) {
			//logger.info("1");
			DatagramPacket responsePacket = prepareClientResponsePacket(syncRequest);
			getServerUdpSocket().send(responsePacket);
			if (!syncRequest.getMainOp().equals("query")) {
				//logger.info("2");
				if (!transition.get()) {
					if (!this.isHead()) {
						//logger.info("3");
						syncRequest.setOp("sync_ack");
						sendPredecessorRequest(syncRequest);
						logger.info("Tail sent ack for request "+syncRequest.getServerSequence());
					}
				} else {
					logger.info("Request "+syncRequest.getServerSequence()+""
							+ " added to sent as tail is in transition");
					Sent.add(syncRequest);
				}
			}
		} else {
			if (!(successorIp.equals("") && successorTcpPort == 0) &&
					!this.getStopSync()) {
				sendSuccessorRequest(syncRequest);
				logger.info("Request "+syncRequest.getServerSequence()+""
						+ " added to sent");
				Sent.add(syncRequest);
			}
		}
	}

	public Request serverReceiveRequest() throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		setServerTcpSoTimeout(60);
		Socket sock = getServerTcpSocket().accept();
		ObjectInputStream inStream = new ObjectInputStream(sock.getInputStream());
		return (Request)inStream.readObject();
	}
	public void sendSuccessorRequest(Request syncRequest) throws UnknownHostException, IOException {
		Socket successorSock = new Socket(successorIp,successorTcpPort);
		ObjectOutputStream outStream = new ObjectOutputStream(successorSock.getOutputStream());
		outStream.writeObject(syncRequest);
		successorSock.close();
	}
	public void sendPredecessorRequest(Request syncRequest) throws UnknownHostException, IOException {
		logger.info(Integer.toString(predecessorTcpPort));
		Socket successorSock = new Socket(predecessorIp,predecessorTcpPort);
		ObjectOutputStream outStream = new ObjectOutputStream(successorSock.getOutputStream());
		outStream.writeObject(syncRequest);
		successorSock.close();
	}
	public void setBalance(int index, int amount) {
		// TODO Auto-generated method stub
		synchronized(accountList) {
			accountList.get(index).setBalance(amount);
		}
	}

	public void addProcessedRequest(int index, ProcessedRequest processedRequest) {
		// TODO Auto-generated method stub
		synchronized(accountList) {
			accountList.get(index).addProcessedRequest(processedRequest);
		}
	}
	public void printDetails() {
		synchronized (accountList) {
			for(Iterator<Account> iter = accountList.iterator(); iter.hasNext();){
				Account account = iter.next();
				logger.info("Account Number:"+account.getAccNum());
				logger.info("Account Balance: "+account.getBalance());
				List<ProcessedRequest> history = account.getHistory();
				logger.info("Account Process History:");
				logger.info("--------------------------------------------------------");
				for (Iterator<ProcessedRequest> hiter = history.iterator(); hiter.hasNext();){
					ProcessedRequest preq = hiter.next();	
					logger.info("Transaction "+preq.getOp());
					logger.info("Transaction ID:"+preq.getRequestID());
				}
				logger.info("--------------------------------------------------------");
			}
		}
	}
	public void printSent() {
		synchronized (Sent) {
			for(Iterator<SyncRequest> iter = Sent.iterator(); iter.hasNext();){
				SyncRequest request = iter.next();
				logger.info("--------------------------------------------------------");
				logger.info(request.getMainOp()+" "+request.getAccount()+" "+request.getAmount()
						+" "+request.getRequestID()+" "+request.getServerSequence());
				logger.info("--------------------------------------------------------");
			}
		}
	}

	public int incrReqSequence() {
		// TODO Auto-generated method stub
		if (this.reqSequence != null)
			return this.reqSequence.incrementAndGet();
		return 0;
	}

	public int sentContainsServerSequence(SyncRequest syncRequest) {
		// TODO Auto-generated method stub
		synchronized (Sent) {
			for(Iterator<SyncRequest> iter = Sent.iterator(); iter.hasNext();) {
				SyncRequest request = iter.next();
				if (request.getServerSequence() == syncRequest.getServerSequence()) {
					return Sent.indexOf(request);
				}
			}
		}
		return -1;
	}

	public void removeAllBefore(int index) {
		// TODO Auto-generated method stub
		synchronized (Sent) {
			for (int i = 0; i <= index; i++)
				if (!Sent.isEmpty())
					Sent.remove(0);
		}
	}

	public String getMasterIp() {
		return masterIp;
	}

	public void setMasterIp(String masterIp) {
		this.masterIp = masterIp;
	}

	public int getMasterTcpPort() {
		return masterTcpPort;
	}

	public void setMasterTcpPort(int masterTcpPort) {
		this.masterTcpPort = masterTcpPort;
	}

	public int getMasterUdpPort() {
		return masterUdpPort;
	}

	public void setMasterUdpPort(int masterUdpPort) {
		this.masterUdpPort = masterUdpPort;
	}
	public DatagramPacket preparePingRequestPacket() throws IOException {
		MasterRequest request = new MasterRequest("ping",this.getBankName());
		ByteArrayOutputStream responseArrayStream = new ByteArrayOutputStream();
		ObjectOutputStream outStream = new ObjectOutputStream(responseArrayStream);
		outStream.writeObject(request);
		byte[] byteResponse = responseArrayStream.toByteArray();
		byte[] byteResponseStream = new byte[4096];
		System.arraycopy(byteResponse, 0, byteResponseStream, 0, byteResponse.length);
		return new DatagramPacket(byteResponseStream, byteResponseStream.length,
				InetAddress.getByName(masterIp), masterUdpPort);
	}
	public void pingMaster() throws IOException {
		// TODO Auto-generated method stub
		DatagramPacket pingPacket = preparePingRequestPacket();
		getServerUdpSocket().send(pingPacket);
	}

	public void clearSent() {
		// TODO Auto-generated method stub
		this.Sent.clear();
	}

	public void sendMaster(MasterRequest request) throws UnknownHostException, IOException {
		// TODO Auto-generated method stub
		Socket successorSock = new Socket(masterIp,masterTcpPort);
		ObjectOutputStream outStream = new ObjectOutputStream(successorSock.getOutputStream());
		outStream.writeObject(request);
		successorSock.close();
	}

	public boolean getStopSync() {
		return stopSync.get();
	}

	public void setStopSync(boolean b) {
		if(this.stopSync == null)
			this.stopSync = new AtomicBoolean(b);
		else
			this.stopSync.set(b);
	}

	public int getSentIndex(int i) {
		// TODO Auto-generated method stub
		synchronized (Sent) {
			for (Iterator<SyncRequest> iter = Sent.iterator();iter.hasNext();){
				SyncRequest request = iter.next();
				if (request.getServerSequence() == i)
					return Sent.indexOf(request);
			}
			return -1;
		}
	}

	public void transmitMissedRequests(int index) throws UnknownHostException, IOException {
		// TODO Auto-generated method stub
		synchronized(Sent){
			logger.info("Transmitting missing requests starting from"
					+ ""+index);
			int i = 0;
			for(Iterator<SyncRequest> iter = Sent.iterator();iter.hasNext();) {
				if (i < index) {
					iter.next();
				} else {
					SyncRequest request = iter.next();
					if (!(successorIp.equals("") && successorTcpPort == 0)) {
						sendSuccessorRequest(request);
					}
				}
				i++;
			}
		}
	}

	public int getFailPred() {
		return failPred;
	}

	public void setFailPred(int failPred) {
		this.failPred = failPred;
	}

	public int getFailSucc() {
		return failSucc;
	}

	public void setFailSucc(int failSucc) {
		this.failSucc = failSucc;
	}

	public int getFailExt() {
		return failExt;
	}

	public void setFailExt(int failExt) {
		this.failExt = failExt;
	}

	public int getJoinRetries() {
		return joinRetries;
	}

	public void setJoinRetries(int joinRetries) {
		this.joinRetries = joinRetries;
	}

	public int getJointTimeout() {
		return jointTimeout;
	}

	public void setJointTimeout(int jointTimeout) {
		this.jointTimeout = jointTimeout;
	}
	public int getKilledNewSuccessor() {
		return killedNewSuccessor;
	}
	public void incrKilledNewSuccessor() {
		killedNewSuccessor++;
	}

	public int getFailCurr() {
		return failCurr;
	}

	public void setFailCurr(int failCurr) {
		this.failCurr = failCurr;
	}
}
