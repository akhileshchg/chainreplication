Chain Replication
=================
An asynchronous replicated backup service for banking transsactions.
Every bank has a chain of servers, the first one called head and the
last one: tail. 
A client sends a transaction request: deposit, withdraw, transfer or
query, to the head which is carried if the transaction is 'good' and
the result is forwarded to successor until the tail. The tail then
responds back to the response to client. The system also has ability
to handle any failures of servers in between or the head or the tail. 
Also new servers can be added to the chain while the transactions are
being processed.

The pseudocode in pseudocode/ will give the detailed picture of the
code organized as client, server and master

Languages Used:
==============
Seperate implementation both in DistAlgo and Java

Test cases
==========
Various test cases are written for testing conditions ranging from
check the backing up the transactions to failure of servers and
introduction of new servers to the chains of banks. They can be found
in distalgo/configs and java/configs for DistAlgo and Java
respectively

Execution
=========
DistAlgo
--------
change directory to src/chain
dar chain.da <config file path>

Java
-----
go to bin/
java -jar server.jar <config file path> <bank number> <server number>
This should be done for all the servers in all the banks

java -jar chain.jar <config file path>

